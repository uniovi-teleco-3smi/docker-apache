# Contenedor con Apache para la asignatura Servicios Multimedia e Interactivos

Clona el repositorio y modifícalo según tus preferencias.

### Parámetros que pueden modificarse

 * Redirección del puerto de escucha del contenedor (80) al del sistema anfitrión.

Los parámetros deben cambiarse en el fichero 'docker-compose.yml' antes de iniciar el contenedor.

### Arrancar el contenedor

Para iniciar el contenedor (ejecutar su imagen):

```console
sudo docker-compose up -d
```

Para parar/arrancar/reiniciar el contenedor:

```console
sudo docker-compose stop/start/restart
```

El contenedor volverá a arrancar automáticamente si se reinicia la máquina anfitriona.